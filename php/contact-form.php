<?php

require_once('Contact.class.php');

$contactPerson = new Contact();

$contactDataArray = array();
$contactErrorsArray = array();

// load the contact if we have it
if (isset($_REQUEST['contactID']) && $_REQUEST['contactID'] > 0) 
{
    $contactPerson->load($_REQUEST['contactID']);
    $contactDataArray = $contactPerson->contactData;
}

if (isset($_POST['reset'])) 
{
    header("location: index.php#contact");
    exit;
}

// apply the data if we have new data
if (isset($_POST['submit']))
{
    $contactDataArray = $_POST;

    // sanitize
    $contactDataArray = $contactPerson->santinize($contactDataArray);
    $contactPerson->set($contactDataArray);

    // validate
    if ($contactPerson->validate())
    {
        // save
        if ($contactPerson->save())
        {
			if ($contactPerson->sendContactEmail() && $contactPerson->sendOwnerEmail())
			{
				header("location: index.php#contact");
            	exit;
			}
			else
        	{
				$contactErrorsArray[] = "Email failed";
        	}
        }
        else
        {
			$contactErrorsArray[] = "Save failed";
        }
    }
    else
    {
		$contactErrorsArray = $contactPerson->errors;
		$contactDataArray = $contactPerson->contactData;
    }
}
function echoValue($dataArray, $key) 
{
    return (isset($dataArray[$key]) ? $dataArray[$key] : '');
}
?>
