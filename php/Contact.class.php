<?php

class Contact 
{
    var $contactData = array();
    var $errors = array();

    var $db = null;

    function __construct() 
    {
        $this->db = new PDO('mysql:host=localhost;dbname=personal-portfolio;charset=utf8', 'EAV', 'Adelyn12');   
        //$this->db = new PDO('mysql:host=localhost;dbname=personal-portfolio;charset=utf8', 'root', '');  
        //$this->db = new PDO('mysql:host=localhost;dbname=eavance_wdv341;charset=utf8', 'eavance_wdv341', 'Adelyn12!');   
    }
    
    function set($dataArray)
    {
        $this->contactData = $dataArray;
    }
    
    function santinize($dataArray)
    {
        // sanitize data based on rules
        $dataArray['contactName'] = filter_var($dataArray['contactName'], FILTER_SANITIZE_STRING);
        $dataArray['contactEmail'] = filter_var($dataArray['contactEmail'], FILTER_SANITIZE_EMAIL);
        $dataArray['contactPhone'] = filter_var($dataArray['contactPhone'], FILTER_SANITIZE_NUMBER_INT);
        $dataArray['contactQuestions'] = filter_var($dataArray['contactQuestions'], FILTER_SANITIZE_STRING);

        return $dataArray;
    }
    
    function load($contactID)
    {
        $isLoaded = false;

        // load from database                
        $stmt = $this->db->prepare("SELECT * FROM portfolio_contacts WHERE contactID=?");
        $stmt->execute(array($contactID));

        if ($stmt->rowCount() == 1) 
        {
            $dataArray = $stmt->fetch(PDO::FETCH_ASSOC);
            //var_dump($dataArray);
            $this->set($dataArray);
            
            $isLoaded = true;
        }
        
        //var_dump($stmt->rowCount());
                
        return $isLoaded;
    }
    
    function save() 
    {
        $isSaved = false;
        
        // determine if insert or update based on contactID
        // save data from contactData property to database
        if (empty($this->contactData['contactID']))
        {
            $stmt = $this->db->prepare(
                "INSERT INTO portfolio_contacts 
                    (contactName, contactEmail, contactPhone, contactQuestions) 
                 VALUES (?, ?, ?, ?)");
               
            $isSaved = $stmt->execute(array(
                    $this->contactData['contactName'],
                    $this->contactData['contactEmail'],
                    $this->contactData['contactPhone'],
                    $this->contactData['contactQuestions']
                )
            );
            
            if ($isSaved) 
            {
                $this->contactData['contactID'] = $this->db->lastInsertId();
            }
        } 
        else 
        {
            $stmt = $this->db->prepare(
                "UPDATE portfolio_contacts SET 
                    contactName = ?,
                    contactEmail = ?,
                    contactPhone = ?,
                    contactQuestions = ?
                WHERE contactID = ?"
            );
                    
            $isSaved = $stmt->execute(array(
                    $this->contactData['contactName'],
                    $this->contactData['contactEmail'],
                    $this->contactData['contactPhone'],
                    $this->contactData['contactQuestions'],
                    $this->contactData['contactID']
                )
            );            
        }
                        
        return $isSaved;
    }

    function sendContactEmail() 
    {
        $sendSuccess = false;

        //email to person who submitted the contact form
        $message = "Thank you for contacting me " .$this->contactData['contactName']. "." ." \n";
        $message .= "I have received the following information:" . " \n\n";
        
        $message .= "NAME: " . $this->contactData['contactName']. " \n";
        $message .= "EMAIL: " . $this->contactData['contactEmail']. " \n";
        $message .= "PHONE: " . $this->contactData['contactPhone']. " \n";
        $message .= "QUESTIONS: " . $this->contactData['contactQuestions']. " \n\n";

        $message .= "I will be in contact with you shortly." . " \n\n";

        $message .= "Erin Vance" . " \n" . "erinavance@yahoo.com" . " \n" . "http://erinvance.info/" . " \n" . "573-953-2343" . " \n\n";

        $emailBody = wordwrap($message,65,"\n",FALSE);

        $headers = "MIME-Version: 1.0" . " \r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1" . " \r\n";
		$headers .= "From: contact@erinvance.info" . " \n";

        $sendSuccess = mail($this->contactData['contactEmail'] , "Your contact info has been received", $emailBody, $headers);
        return $sendSuccess;
    }

    function sendOwnerEmail() 
    {
        $sendSuccess = false;

        //email to the website owner with the contact info from the user
        $message = "The below contact info has been received from the website erinvance.info." . " \n\n";

        $message .= "Please contact: " . " \n\n";
        
        $message .= "NAME: " . $this->contactData['contactName']. " \n";
        $message .= "EMAIL: " . $this->contactData['contactEmail']. " \n";
        $message .= "PHONE: " . $this->contactData['contactPhone']. " \n";
        $message .= "QUESTIONS: " . $this->contactData['contactQuestions']. " \n\n";

        $emailBody = wordwrap($message,65,"\n",FALSE);

        $headers = "MIME-Version: 1.0" . " \r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1" . " \r\n";
		$headers .= "From: contact@erinvance.info" . " \n";
 
        $sendSuccess = mail("erinavance@yahoo.com" , "Erin Vance Please Contact", $emailBody, $headers);
        return $sendSuccess;
    }

    function validate()
    {
        $isValid = true;
        
        // if an error, store to errors using column name as key
        
        // validate the data elements in contactData property

        //name is required
        if (empty($this->contactData['contactName']))
        {
            $this->errors['contactName'] = "Required";
            $isValid = false;
        }  

        //email is required, should be in format abc@yahoo.com
        if (empty($this->contactData['contactEmail']))
        {
            $this->errors['contactEmail'] = "Required";
            $isValid = false;
        }  
        else if (!filter_var($this->contactData['contactEmail'], FILTER_VALIDATE_EMAIL))
        {
            $this->errors['contactEmail'] = "Invalid Format (abc@yahoo.com)";
            $isValid = false;
        }

        //phone is required, should be in format of 555-555-5555
        if (empty($this->contactData['contactPhone']))
        {
            $this->errors['contactPhone'] = "Required";
            $isValid = false;
        } 
        else if (!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $this->contactData['contactPhone']))
        {
            $this->errors['contactPhone'] = "Invalid Format (555-555-5555)";
            $isValid = false;
        }
        
        //questions is required
        if (empty($this->contactData['contactQuestions']))
        {
            $this->errors['contactQuestions'] = "Required";
            $isValid = false;
        }
       
        return $isValid;
    }
    
}
?>