<?php
session_start();

if (isset($_SESSION['validUser'])) {             //if signed on then continue with access to the page
	  
	$category = $brand = $name = $abv = $ibu = $notes = $image = $rating = $favorite = $image = "";    
	$imageUploadError = $message = $e = "";  
	$validForm = false;

	require_once('files/connectPDO.php');	    //CONNECT to the database

	if($_SERVER['REQUEST_METHOD'] == 'POST'){      //IF FORM HAS HAS BEEN SUBMITTED, GATHER INPUT
		$category = $_POST["beer_category"];		
		$brand = $_POST["beer_brand"];		
		$name = $_POST["beer_name"];			
		$abv = $_POST["beer_abv"];
		$ibu = $_POST["beer_ibu"];
		$notes = $_POST["beer_notes"];
		$image = $_FILES["beer_image"]["name"];
		$rating = $_POST["beer_rating"];
		$favorite = (isset($_POST['beer_favorite'])) ? 'Yes' : 'No';      //if beer favorite is selected then yes is the value, if not selected then no is the value passed
	
		//validate upload image
		function validateUploadImage(){
			global $validForm, $imageUploadError;

			if(isset($_FILES["beer_image"]) && $_FILES["beer_image"]["error"] == 0){            //if adding new image check the following
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
				$filename = $_FILES["beer_image"]["name"];
				$filetype = $_FILES["beer_image"]["type"];
				$filesize = $_FILES["beer_image"]["size"];
				
				// Verify file extension
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
				
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
				
				// Verify MYME type of the file
				if(in_array($filetype, $allowed)){
					// Check whether file exists before uploading it
					if(file_exists("images/database_images/" . $_FILES["beer_image"]["name"])){
						$validForm = false;
						$imageUploadError = $_FILES["beer_image"]["name"] . " already exists in desired folder.";
					}else{
						move_uploaded_file($_FILES["beer_image"]["tmp_name"], "images/database_images/" . $_FILES["beer_image"]["name"]);  //move image file to images folder
					}
				}else{
					$validForm = false;
					$imageUploadError = "Error: There was a problem uploading your file. Please try again."; 
				}
			}
		}

		$validForm = true;       
		validateUploadImage();
			
		if($validForm){             //IF VALID FORM MOVE CONTENT TO DATABASE
				
				try {
					//Create the SQL command string
					$sql = "INSERT INTO beer_inputs (";    
					$sql .= "beer_category, ";             
					$sql .= "beer_brand, ";
					$sql .= "beer_name, ";
					$sql .= "beer_abv, ";
					$sql .= "beer_ibu, ";
					$sql .= "beer_notes, ";
					$sql .= "beer_image, ";
					$sql .= "beer_rating, ";
					$sql .= "beer_favorite ";   
					$sql .= ") VALUES (:category, :brand, :name, :abv, :ibu, :notes, :image, :rating, :favorite)";
					
					//PREPARE the SQL statement
					$stmt = $conn->prepare($sql);
					
					//BIND the values to the input parameters of the prepared statement
					$stmt->bindParam(':category', $category);
					$stmt->bindParam(':brand', $brand);		
					$stmt->bindParam(':name', $name);		
					$stmt->bindParam(':abv', $abv);		
					$stmt->bindParam(':ibu', $ibu);
					$stmt->bindParam(':notes', $notes);
					$stmt->bindParam(':image', $image);
					$stmt->bindParam(':rating', $rating);
					$stmt->bindParam(':favorite', $favorite);
					
					//EXECUTE the prepared statement
					$stmt->execute();	
					$message = "<span style='color:#329932; padding-right:1%;'>&#x2714;</span>Product Successfully Added";
					
					$conn = null;
				}catch(PDOException $e){
					$message = "<span style='color:#660000; padding-right:1%;'>&#x2718;</span> There has been a problem. Please try again later.";
				}
				
		}else{                     //IF INVALID FORM DISPLAY ERROR MESSAGE & FORM
			$message = "<span style='color:#b20000;'>Invalid Entry. Please Try Again.</span>";
		}//ends check for valid form	
	}
}else{
	header('Location: login.php');         //else if you are not signed on then redirect to login
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BEERate Project</title>
  <link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
  <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto+Condensed:700" rel="stylesheet">  
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="files/beerJqueryUI/jquery-ui-1.12.1.custom/jquery-ui.min.css">
  <!--Custom CSS--> 
  <link rel="stylesheet" href="files/mainStyles.css">
  <link rel="stylesheet" href="files/addStyles.css">
  <script src="files/jquery-3.2.1.min.js"></script>
  <script src="files/beerJqueryUI/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
  <script>
		$(document).ready(function(){
		
		  //Character count for textarea
			var max = 300;
			$(".noteBox").keyup(function(e){
				$("#count").text("Characters left: " + (max - $(this).val().length));   //displays how many characters they have left out of 300
			});
		
		  //on click of up arrow animated scroll to top
			$('#topButton').click(function(){
				$("html, body").animate({      //body is used by webkit browsers, html is used by firefox
					scrollTop:0  
				}, 1000)                    //speed
			});
			
		 //ABV slider - JQuery UI	
			$(function(){
				$("#sliderABV").slider({
				  value:0,                 //defualt value on start
				  min: 0.0,                //slider starts at 0
				  max: 17.0,               //slider ends at 17
				  step: .1,                //increments of .1
				  slide: function(event, ui) {
					$("#amount1").val(ui.value + ' %');
				  }
				});
				$("#amount1").val($("#sliderABV").slider("value") + ' %');   //outputs selected value
			});
			
		 //IBU slider - JQuery UI	
			$(function(){
				$("#sliderIBU").slider({
				  value:0,              //defualt value on start
				  min: 0,               //slider starts at 0
				  max: 100,	            //slider ends at 100
				  step: 1,             //increments of 1
				  slide: function(event, ui) {
					$("#amount2").val(ui.value + ' units');
				  }
				});
				$("#amount2").val($("#sliderIBU").slider("value") + ' units');   //outputs selected value
			});
			
		 //Star Rating	
			$('.rating input').change(function () {              //on star rating radio button change do the following
				var $radio = $(this);
				$('.rating .selected').removeClass('selected');  //take the previously selected star rating and remove that selected class
				$radio.closest('label').addClass('selected');    //take the current selected star rating and add selected class
			});
			
		  //VALIDATIONS FOR ADD BEER FORM
			var category = brand = name = abv = ibu = notes = rating = favorites = '';
			var validForm = true;
			
			function validateCategory(category){
				if(category == null){                        //category if unselected is invalid
					validForm = false;
					$('#categoryError').html('Category is Required');
				}
			}
			
			function validateBrand(brand){
				var pattern = /^[a-zA-Z0-9-\s]*$/;           //only allows numbers 0-9, a-z, A-Z, dash and paired with spaces
				if(brand == '' || /^\s+$/.test(brand)){      //if just spaces or blank it is invalid entry
					validForm = false;
					$('#brandError').html('Brand is Required');
				}else if(!pattern.test(brand)){
					validForm = false;
					$('#brandError').html('Only numbers 0-9 and Aa-Zz allowed');
				}
			}
			
			function validateName(name){
				var pattern = /^[a-zA-Z0-9-\s]*$/;          //only allows numbers 0-9, a-z, A-Z, dash and paired with spaces
				if(name == '' || /^\s+$/.test(name)){       //if just spaces or blank it is invalid entry
					validForm = false;
					$('#nameError').html('Name is Required');
				}else if(!pattern.test(name)){
					validForm = false;
					$('#nameError').html('Only numbers 0-9 and Aa-Zz allowed');
				}
			}
			
			function validateNotes(notes){
				if(notes == '' || /^\s+$/.test(notes)){         //if just spaces or blank it is invalid entry
					validForm = false;
					$('#notesError').html('Notes is Required');
				}else if(/[<>]/g.test(notes)){                 //doesnt allow < >
					validForm = false;
					$('#notesError').html('Cannot contain these characters < > ');
				}
			}
			
			function validateRate(rating){
				if(rating == undefined){                      //if star rating is unchecked it is invalid
					validForm = false;
					$('#ratingError').html('Rating is Required');
				}
			}
			
			$("#submitBtn").click(function() {                 //on submit button click gather user input
				category = $(".dropdown").val();
				brand = $("#brandBox").val();
				name = $("#nameBox").val();
				abv = $("#amount1").val();
				ibu = $("#amount2").val();
				notes = $('.noteBox').val();
				rating = $("input[name='beer_rating']:checked").val();
				favorites = $("input[name='beer_favorite']:checked").val();
				
				$("#categoryError,#brandError,#nameError,#notesError,#ratingError").html("");   //clear previous displayed errors

				validForm = true;
				validateCategory(category);                   //call validation funtions to validate user input
				validateBrand(brand);
				validateName(name);
				validateNotes(notes);
				validateRate(rating);
				
				if(validForm){                 //if validform submit form
					$('#addForm').submit();
				}
			});
		});
  </script>
</head>
<body>
 
	<h1><img src="images/beerLogoSM.png" width="50" height="66" class='logo'/> BEERate</h1>
	<h3><em><?php echo $message; ?></em></h3> 
	<h3><em><?php echo $imageUploadError; ?></em></h3>
	
	<ul id="navigationMenu">
		<li><a class="browse" href="index.php"><i class="fa fa-list fa-lg" aria-hidden="true"></i><span>Browse</span></a></li> 
		<li><a class="favorite" href="index.php#Favs"><i class="fa fa-heart fa-lg" aria-hidden="true"></i><span>Favorites</span></a></li>
		<li><a class="add" id="active" href="add.php"><i class="fa fa-plus fa-lg" aria-hidden="true"></i><span>Add Beer</span></a></li>
		<li><a class="login" href="login.php"><i class="fa fa-sign-in fa-lg" aria-hidden="true"></i><span>Login</span></a></li>
		<li><a class="logout" href="logout.php"><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i><span>Logout</span></a></li>
	</ul>
		
	<div id="formContainer">
		<h2>Add Beer</h2>
           <form method="post" name="addForm" id='addForm' action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" enctype="multipart/form-data">
		   
                <div class='sec'> 
					<label class='col2'>Category <span id='categoryError'></span></label>
					<div class='col10'>
						<select name='beer_category' class="dropdown">
							<option value="default" selected disabled> - Select - </option>
							<option value="Saison">Saison</option>
							<option value="Ale">Ale</option>
							<option value="Wheat">Wheat</option>
							<option value="IPA">IPA</option>
							<option value="Lager">Lager</option>
							<option value="Barley">Barley</option>
							<option value="Porter">Porter</option>
							<option value="Stout">Stout</option>
							<option value="Other">Other</option>
						</select>
					</div>
				</div> 
				
                <div class='sec'>
					<label class='col2'>Brand <span id='brandError'></span></label>
					<div class='col10'>
						<input name="beer_brand" type="text" class="textBox" id='brandBox'/>
					</div>
				</div> 
				
				<div class='sec'>
					<label class='col2'>Name <span id='nameError'></span></label>
					<div class='col10'>
						<input name="beer_name" type="text" class="textBox" id='nameBox'/>
					</div>
				</div>
				
				<div class='sec'>
					<label class='col2'>ABV </label>
					<div class='col10'>
						<label for='amount1'><input type="text" id="amount1" name="beer_abv" readonly style="border:0; color:#000000;"></label>
						<div id="sliderABV" ></div>
					</div>
				</div>
				
				<div class='sec'>
					<label class='col2'>IBU </label>
					<div class='col10'>
						<label for='amount2'><input type="text" id="amount2" name="beer_ibu" readonly style="border:0; color:#000000;"></label>
						<div id="sliderIBU" ></div>
					</div>
				</div>
				
				<div class='sec'>
					<label class='noteLabel'>Notes <span id='notesError'></span></label>
					<div class='col12'>
						<textarea name="beer_notes" type="text" rows="7" class='noteBox' maxlength="300"></textarea>
						<div id="count"></div>
					</div>
				</div>
				
				<div class='sec'>
					<label class='col2'>Select Product Image<br><span class="error"></span></label>
					<div class='col10'>
						<input type="file" name="beer_image" id="fileSelect"/>
						<label for="fileSelect"></label>
						<p class="note"><em> Only .jpg, .jpeg, .gif, .png formats allowed. (Max size 5 MB)</em></p>
					</div>
				</div>
				
				<div class='rating'>
					<p>Rating <br><span id='ratingError'></span></p>
					<label>
						<input type="radio" name="beer_rating" value="5" title="5 stars"> 5
					</label>
					<label>
						<input type="radio" name="beer_rating" value="4" title="4 stars"> 4
					</label>
					<label>
						<input type="radio" name="beer_rating" value="3" title="3 stars"> 3
					</label>
					<label>
						<input type="radio" name="beer_rating" value="2" title="2 stars"> 2
					</label>
					<label>
						<input type="radio" name="beer_rating" value="1" title="1 star"> 1
					</label>
				</div>
				
				<div class='favs'>
					<input type="checkbox" id='box1' name='beer_favorite' value='Yes'/>
					<label class="favCheckbox" for='box1'>Add to Favorites </label>
				</div>
		
                <p class="formButtons">
					<input name="submitBtn" value="SUBMIT" type="button" class="btn" id='submitBtn'/> 
					<input name="" type="reset" value="RESET" class="btn"/>&nbsp;
				</p>
				
           </form>
	</div>

	<p id="topButton"><a href="#"><i class="fa fa-chevron-up fa-lg" aria-hidden="true"></i></a></p>
 
</body>
</html>