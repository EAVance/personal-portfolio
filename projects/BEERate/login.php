<?php
session_cache_limiter('none');			
session_start();
 
	if (isset($_SESSION['validUser'])){	                    //if valid user or already signed on, skip the rest
		header('Location: index.php');
		exit;
	}else{
		$messageError = '';
		if (isset($_POST['submitLogin']) ){			         //if login portion was submitted do the following
		
			$inUsername = $_POST['event_user_name'];	    //pull the username from the form
			$inPassword = $_POST['event_user_password'];	//pull the password from the form

			require_once('files/connectPDO.php');				        //Connect to the database

			$stmt = $conn->prepare("SELECT event_user_name,event_user_password FROM event_user WHERE event_user_name = ? AND event_user_password = ?"); 
			$stmt->execute(array($inUsername, $inPassword));

			$stmt->fetch(PDO::FETCH_ASSOC);	
			
			if ($stmt->rowCount() == 1 ){		           //If this is a valid user there should be ONE row only
				$_SESSION['validUser'] = true;				//this is a valid user so set your SESSION variable
				header('Location: index.php');
				exit;
			}else{	
				$messageError = "<em style='color:#b20000;'>Incorrect Username or Password. <br>Please try again.</em>";     //error in login, not valid user
			}			
			
			$conn = null;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BEERate Project</title>
  <link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
  <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto+Condensed:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!--Custom CSS--> 
  <link href="files/mainStyles.css" rel="stylesheet">
  <link href="files/loginStyles.css" rel="stylesheet">
</head>
<body>	
		<h1><img src="images/beerLogoSM.png" width="50" height="66" class='logo'/> BEERate</h1>
		<h3><?php echo $messageError;?></h3>
		
		<div id="loginContainer">
			<h2>Admin Login</h2>
            <form method="post" name="loginForm" action="login.php" >
                <p> 
					<label>Username:</label>
					<input name="event_user_name" type="text" class="form-control"/>
				</p> 
                <p>
					<label>Password:</label>
					<input name="event_user_password" type="password" class="form-control" />
				</p> 
                <p class="formButtons">
					<input name="submitLogin" value="LOGIN" type="submit" class="btn"/> 
					<input name="" type="reset" value="RESET" class="btn"/>&nbsp;
				</p>
            </form>
		</div>         
</body>
</html>