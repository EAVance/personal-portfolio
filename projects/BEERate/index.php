<?php
session_cache_limiter('none');			
session_start();               

if (isset($_SESSION['validUser'])) {    //if signed on then continue with access to the page
	
	require_once('files/connectPDO.php');	   //database connection
	$displayErrorMsg = "";
	
	try {    //get database info according to their beer_category
		$sqlSaison = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Saison'");
		$sqlSaison->execute();
		
		$sqlAle = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Ale'");
		$sqlAle->execute();
		
		$sqlWheat = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Wheat'");
		$sqlWheat->execute();
		
		$sqlIPA = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'IPA'");
		$sqlIPA->execute();
		
		$sqlLager = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Lager'");
		$sqlLager->execute();
		
		$sqlBarley = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Barley'");
		$sqlBarley->execute();
		
		$sqlPorter = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Porter'");
		$sqlPorter->execute();
		
		$sqlStout = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Stout'");
		$sqlStout->execute();
		
		$sqlOther = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_category = 'Other'");
		$sqlOther->execute();

		//get only database info that has the beers listed as favorites
		$sqlFav = $conn->prepare("SELECT beer_category, beer_brand, beer_name, beer_abv, beer_ibu, beer_notes, beer_image, beer_rating, beer_favorite FROM beer_inputs WHERE beer_favorite = 'Yes'");
		$sqlFav->execute();
		
		
	}catch(PDOException $e){
		$displayErrorMsg = "<em>Sorry there has been a problem.</em>";
		$conn = null;
	}
	
}else{
	header('Location: login.php');   //else if you are not signed on then redirect to login
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BEERate Project</title>
  <meta name="description" content="Erin Vance Web Development Project - BEERate">
  <link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
  <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto+Condensed:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="files/beerJqueryUI/jquery-ui-1.12.1.custom/jquery-ui.min.css">
  <!--Custom CSS--> 
  <link rel="stylesheet" href="files/mainStyles.css">
  <link rel="stylesheet" href="files/indexStyles.css">
  <script src="files/jquery-3.2.1.min.js"></script>
  <script src="files/beerJqueryUI/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
  <script>
		$(document).ready(function() {
		  //on click of up arrow animated scroll to top
			$('#topButton').click(function() {
				$("html, body").animate({
					scrollTop:0 
				}, 1000)             //speed 
			});
			
		 //on click of the favorite accordion tab or favorite navigation link, the favorite navigation becomes active 
			$('#Favs,.favorite').click(function(){
				$('.favorite').attr('id','active');
				$('.browse').removeAttr('id');       
			});
			
		  //JQUERY UI accordion
			$(function(){
				var icons = {                          //icons for tabs
				  header: "ui-icon-triangle-1-e",         
				  activeHeader: "ui-icon-triangle-1-s" 
				};
				$( "#accordion" ).accordion({
				  icons: icons,                       //allows for icons to be on
				  heightStyle: "content",             //opens current tab to full height of content within
				  collapsible: true,                  //collapsible tabs on click
				  active: false,                      //initially all collapsed
				  activate: function( event, ui ) {               //scrolls to top of active section when activated
					if(!$.isEmptyObject(ui.newHeader.offset())) {
						$('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
					}
				  }
				});
			});
		});
  </script>
</head>
<body>
 
	<h1><img src="images/beerLogoSM.png" width="50" height="66" class='logo'/> BEERate</h1>

	<ul id="navigationMenu">
		<li><a class="browse" id="active" href="index.php"><i class="fa fa-list fa-lg" aria-hidden="true"></i><span>Browse</span></a></li> 
		<li><a class="favorite" href="#Favs"><i class="fa fa-heart fa-lg" aria-hidden="true"></i><span>Favorites</span></a></li>
		<li><a class="add" href="add.php"><i class="fa fa-plus fa-lg" aria-hidden="true"></i><span>Add Beer</span></a></li>
		<li><a class="login" href="login.php"><i class="fa fa-sign-in fa-lg" aria-hidden="true"></i><span>Login</span></a></li>
		<li><a class="logout" href="logout.php"><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i><span>Logout</span></a></li>
	</ul>
	 
	<h4><?php echo $displayErrorMsg; ?></h4>

<div id="accordion-resizer" class="ui-widget-content">
  <div id="accordion">
<!--TAB 1-->
	<h3 id="Saison"> Saison</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlSaison->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>				
	</div>
<!--AB 2-->
    <h3 id="Ale"> Ale</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlAle->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 3-->
	<h3 id="Wheat"> Wheat</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlWheat->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 4-->
	<h3 id="IPA"> IPA</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlIPA->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 5-->
    <h3 id="Lager"> Lager</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlLager->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 6-->
    <h3 id="Barley"> Barley Wine</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlBarley->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 7-->
	<h3 id="Porter"> Porter</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlPorter->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!---TAB 8-->
	<h3 id="Stout"> Stout</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlStout->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>		
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 9-->
	<h3 id="Other"> Other</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlOther->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>			
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
<!--TAB 10-->
	<h3 id="Favs"><i class="fa fa-heart" aria-hidden="true"></i> Favorites</h3>
	<div class="content-container">
	<?php
		//Display each row as formatted output
		while( $row = $sqlFav->fetch()){
			$beerCategory = $row["beer_category"];
			$beerBrand = $row["beer_brand"];
			$beerName = $row["beer_name"];
			$beerABV = $row["beer_abv"]; 
			$beerIBU = $row["beer_ibu"];
			$beerNotes = $row["beer_notes"];
			$beerImage = $row["beer_image"];
			$beerRating = $row["beer_rating"];
			$beerFavorite = $row["beer_favorite"];
	?>	
			<div class="col">
				<div class="beerImg">
					<img src="images/database_images/<?php if($beerImage == ''){ echo 'defualtImg.png';} else{ echo $beerImage;} ?>" alt=''>		
				</div>
				<div class="beerHeading">
					<p><?php if($beerFavorite == 'Yes'){ echo "<i class='fa fa-heart fa-lg' aria-hidden='true'></i>"; }?></p>
					<h2><?php echo $beerBrand; ?></h2>
					<h3><em><?php echo $beerName; ?></em></h3>
					<h3><em><?php echo $beerCategory; ?></em></h3>
					<p><span class='stars'><?php echo str_repeat("<i class='fa fa-star' aria-hidden='true'></i>",$beerRating); ?></span></p>
				</div>	
				<div class="beerNotes">
					<p>
						<ul>
							<li><strong>ABV:</strong> <?php echo $beerABV; ?></li>
							<li><strong>IBU:</strong> <?php echo $beerIBU; ?></li>
						</ul>
						<strong>NOTES: </strong><?php echo $beerNotes; ?>
					</p>
				</div>
			</div>	
	<?php
		}//close while loop
		$conn = null;
	?>	
	</div>
  </div><!--end accordion-->
</div>

	<p id="topButton"><a href="#"><i class="fa fa-chevron-up fa-lg" aria-hidden="true"></i></a></p>
 
</body>
</html>