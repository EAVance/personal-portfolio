<?php
session_start();	

$_SESSION['validUser']=false;  //set validUser to no
session_unset();	//remove all session variables related to current session
session_destroy();	//remove current session

header('Location: login.php');  //when logout redirect to login page
exit;
?>