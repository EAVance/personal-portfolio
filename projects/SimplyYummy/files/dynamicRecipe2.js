$(document).ready(function(){
	
//closes dropdown ingredients & instructions lists if open, returns arrow to initial position, changes servings dropdown to 'original', empties ingredients & instructions lists
	function clearDisplayedRecipe(){
		$(".hiddenIngredients,.hiddenInstructions").css({'display':'none'});
		$(".arrow,.arrow2").removeClass("flip");
		$("#servingsDropdown").val('whole');
		$("#ingredients").empty();
		$("#instructions").empty();
	}
			
//takes given recipe data and displays it in innerHTML by id's
	function displayData(data) {
		$("#recipeName").html(data.name);
		$("#description").html(data.description);
		$("#image").html("<img class='img-fluid' src='" + data.image + "' />");
		$("#yield").html(data.yield);
		$("#preptime").html(data.preptime);
		$("#cooktime").html(data.cooktime);
		$("#difficulty").html(data.difficulty);
		$.each(data.original_ingredients, function(index, value){
			$("#ingredients").append('<li>' + value + '</li>');
		});
		$(data.instructions).each(function(index){
			$("#instructions").append('<li>' + this + '</li>');
		});
	}
		
//empties ingredients list, outputs new given ingredient measurment data back out into the ingredients list
	function displayNewServSize(data){
		$("#ingredients").empty();
		$(data).each(function(index){
			$("#ingredients").append('<li>' + this + '</li>');
		});
	}

//onload initial recipe shown
	$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) {              
		displayData(data.chili);
		$("#currentRecipe").hide().fadeIn('slow','linear');   
	});
				
//on servingsDropdown change, gets new selected serving size data and displays it in ingredients list
	$("#servingsDropdown").change(function(){
		var selectedServing = $(this).val();
		if(selectedServing == "half"){ 
			$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) {   
				if($("#recipeName").html() == "Crockpot Chili"){
					displayNewServSize(data.chili.half_ingredients);
				}else if($("#recipeName").html() == "Pizza Pinwheels"){
					displayNewServSize(data.pizza.half_ingredients);
				}else{
					displayNewServSize(data.verde.half_ingredients);
				}
			});
		}else if(selectedServing == "whole"){
			$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) {   
				if($("#recipeName").html() == "Crockpot Chili"){
					displayNewServSize(data.chili.original_ingredients);
				}else if($("#recipeName").html() == "Pizza Pinwheels"){
					displayNewServSize(data.pizza.original_ingredients);
				}else{
					displayNewServSize(data.verde.original_ingredients);
				}
			});
		}else{
			$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) {   
				if($("#recipeName").html() == "Crockpot Chili"){
					displayNewServSize(data.chili.double_ingredients);
				}else if($("#recipeName").html() == "Pizza Pinwheels"){
					displayNewServSize(data.pizza.double_ingredients);
				}else{
					displayNewServSize(data.verde.double_ingredients);
				}
			});
		}
	});
			
//on thumbnail click, gets selected recipe data and displays it
	$("#chiliThumbnail,#pizzaThumbnail,#verdeThumbnail").click(function(){
		clearDisplayedRecipe();
		if($(this).attr("id") == "chiliThumbnail"){                         
			$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) { 
				displayData(data.chili);
			});
		}else if($(this).attr("id") == "pizzaThumbnail"){                   
			$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) { 
				displayData(data.pizza);
			});
		}else{                                                              
			$.getJSON("http://erinvance.info/projects/SimplyYummy/files/recipes2.json", function (data, status) { 
				displayData(data.verde);
			});
		}
		$("#currentRecipe").hide().fadeIn('slow','linear');
	});
		
//on arrow click rotate arrow 180 degrees and fade in or out hidden ingredients
	$( ".fadeInOut" ).click(function() {
		$(".arrow").toggleClass('flip');
		$(".hiddenIngredients").fadeToggle("slow","linear");
	});
//on arrow click rotate arrow 180 degrees and fade in or out hidden insructions
	$( ".fadeInOut2" ).click(function() {
		$(".arrow2").toggleClass('flip');
		$(".hiddenInstructions").fadeToggle("slow","linear");
	});
			
//on click of up arrow or thumbnail images page has an animated scroll to top
	$('#topButton,#chiliThumbnail,#pizzaThumbnail,#verdeThumbnail').click(function() {
		$("html, body").animate({scrollTop:0 }, 1000)
	});
	
});