<?php
session_start();

if (isset($_SESSION['validUser1'])) {   //if valid user or already signed on you have access to this page
	$inUsername = $_SESSION['userName1'];  //get user name
	$welcomeMsg = "Welcome Back <em>". $inUsername."</em> !";    //welcome message
	
	require_once('files/connectPDO.php');	  //database connection

	$displayMsg = $displayErrorMsg = "";
	$productList = array();
	
	try {     //get all furniture products in database and display in a table for admin to view or select
		$result = $conn->prepare('SELECT product_id, product_name, product_description, product_image_name, date_added, product_active FROM furniture_products');
		$result->execute();
		$productList = $result->fetchAll(PDO::FETCH_ASSOC);
		if(empty($productList)){
			$displayErrorMsg = "Zero results were found";
		}
	}catch(PDOException $e){
		$displayErrorMsg = "Sorry there has been a problem.";  //other database error message
	}
	//if you are not a valid user redirect back to login page
}else{
	header('Location: login.php'); 
	exit;     
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
	<title>Furniture Co</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="files/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">
	<!--Custom CSS--> 
	<link href="files/styles.css" rel="stylesheet">
	<link href="files/selectStyles.css" rel="stylesheet">
	<script src='files/jquery-3.2.1.min.js'></script>
	<script>
		$(document).ready(function() {
		//on click of up arrow animated scroll to top
			$('#topButton').click(function() {
				$("html, body").animate({         
					scrollTop:0 
				}, 1000)                        
			});
		});
	</script>
</head>
<body>

	<nav class="navbar navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php"><img src="images/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li><a href="addProduct.php">Add Product</a></li>
				<li class="active"><a href="selectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $welcomeMsg?></a></li>
				<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Admin</a></li>
				<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
	
	<div class="container mainContent">
		<div class="row">
			<h2>Products</h2>
			<div id="content">
				<table>
					<tr class='tableHeader'><td>ID</td><td>Name</td><td>Description</td><td>Image Name</td><td>Date Added</td><td>Active</td></tr>
<?php      
	//Product table list for desktop size
	foreach ($productList as $product) 
	{
?>
					<tr>
						<td></strong><?php echo $product['product_id']; ?></td>
						<td><?php echo $product["product_name"]; ?></td>
						<td><?php echo $product["product_description"]; ?></td>
						<td><?php echo $product["product_image_name"]; ?></td>
						<td><?php echo $product["date_added"]; ?></td>
						<td><?php echo $product["product_active"]; ?></td>
						<td><a href='addProduct.php?product_id=<?php echo $product['product_id']; ?>' class='tooltip'><i class='fa fa-pencil fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Edit</span></a></td> 
						<td><a href='deleteProduct.php?product_id=<?php echo $product['product_id']; ?>' class='tooltip'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Delete</span></a></td>
					</tr>
<?php
	}
	$conn = null;
?>
				</table>
			</div>
		</div><!--end row-->
	</div> <!-- /container -->



	<div class="container mainContentMobile">
		<div class="row">
			<h2>Products</h2>
<?php
	//Product list for smaller screens
	foreach ($productList as $product) 
	{
?>	
			<p>	
                <a href='addProduct.php?product_id=<?php echo $product['product_id']; ?>'>Edit </a> | <a href='deleteProduct.php?product_id=<?php echo $product['product_id']; ?>'> Delete </a><br>
                <strong>ID</strong>: <?php echo $product['product_id']; ?><br>  
                <strong>Name</strong>: <?php echo $product["product_name"]; ?><br>
                <strong>Description</strong>: <?php echo $product["product_description"]; ?><br>
                <strong>Image Name</strong>: <?php echo $product["product_image_name"]; ?><br>
				<strong>Date Added</strong>: <?php echo $product["date_added"]; ?><br>
				<strong>Active Status</strong>: <?php echo $product["product_active"]; ?>
            </p>
<?php
	}
	$conn = null;
?>		
		</div>
	</div> <!-- /container -->

	<h3><em><?php echo $displayErrorMsg; ?></em></h3>

	<p id="btn2"><a id="topButton" href="#">^</a></p>
	
	<!-- Bootstrap core JavaScript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="files/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>