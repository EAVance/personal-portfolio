<?php
session_cache_limiter('none');			//This prevents a Chrome error when using the back button to return to this page.
session_start();
$messageError = '';
 
	if (isset($_SESSION['validUser1'])){	                    //if valid user or already signed on, skip the rest
		header('location: selectProduct.php');
    exit;	
	}else{
		if (isset($_POST['reset'])) 
		{
			header("location: login.php");
			exit;
		}

		if (isset($_POST['submitLogin']) ){			            //if login portion was submitted do the following
		
			$inUsername = $_POST['event_user_name'];	       //pull the username from the form
			$inPassword = $_POST['event_user_password'];	   //pull the password from the form
			//sanitize
			$inUsername = filter_var($inUsername, FILTER_SANITIZE_STRING);
			$inPassword = filter_var($inPassword, FILTER_SANITIZE_STRING);
			
			require_once('files/connectPDO.php');		  //Connect to the database

			$stmt = $conn->prepare("SELECT event_user_name,event_user_password FROM event_user WHERE event_user_name = ? AND event_user_password = ?"); 
			$stmt->execute(array($inUsername, $inPassword));
			
			$stmt->fetch(PDO::FETCH_ASSOC);
			
			if ($stmt->rowCount() == 1 ){		            //If this is a valid user there should be ONE row only
				$_SESSION['validUser1'] = true;				//this is a valid user so set your SESSION variable
				$_SESSION['userName1'] = $inUsername;
				header('location: selectProduct.php');
        exit;
			}else{
				//error in login, not valid user	
				$messageError = "<em style='color:#b20000;'>Incorrect Username or Password. <br>Please try again.</em>";
			}			
			
			$conn = null;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Erin Vance Web Development Project - Furniture Co.">
	<link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
  <title>Furniture Co</title>
  <!-- Bootstrap core CSS -->
  <link href="files/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <!--Text Styles Google Fonts-->
  <link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">  
  <!--Icons Font Awesome-->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!--Custom CSS--> 
  <link href="files/styles.css" rel="stylesheet">
  <link href="files/loginStyles.css" rel="stylesheet">
</head>
<body>
		
		<h2><a href="index.php"><img src="images/ChairLogo3.png"/>Furniture Co</a></h2>
		<h3><?php echo $messageError;?></h3>
		
		<div id="loginContainer">
			<h3>Administrator Login</h3>
        <form method="post" name="loginForm" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" >
          <label>Username:
						<input name="event_user_name" type="text"/>
					</label> 
          <label>Password:
						<input name="event_user_password" type="password" />
					</label> 
          <p class="formButtons">
						<input name="submitLogin" value="LOGIN" type="submit" class="btn"/> 
						<input name="reset" type="reset" value="RESET" class="btn"/>&nbsp;
					</p>
        </form>
		</div>

		<!-- Bootstrap core JavaScript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="files/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>