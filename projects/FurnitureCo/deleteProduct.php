<?php
session_start();

if (isset($_SESSION['validUser1'])) {       //if valid user you can use this page
	$inUsername = $_SESSION['userName1'];
	$welcomeMsg = "Welcome Back <em>". $inUsername."</em> !"; //displays welcome message once signed in
	
	$deleteProductId = $_GET['product_id'];	//Pull the product_id from the GET parameter	
	
	require_once('files/connectPDO.php');//connects to the database

	$deleteMsg = "";
	
	//find associated image file name with delete request and delete from images folder on server
	$stmt1 = $conn->prepare('SELECT product_image_name FROM furniture_products WHERE product_id = ?');	
	$stmt1->execute([$deleteProductId]);
	$productImageName = $stmt1->fetchColumn();
		if(!empty($productImageName)){
			if(unlink("images/database_product_images/".$productImageName)){   //if successful unlink/delete file from folder 
				$deleteMsg ="<h3><span class='check'>&#x2714;</span> Requested product photo, <em>".$productImageName."</em> was successfully <span style='color:#b20000;'>deleted</span>.</h3>";
			}else{
				$deleteMsg ="<h3><span class='X'>&#x2718;</span> Requested product photo was not deleted.</h3>";
			}
		}
	
	//find requested product name by id 
	$stmt2 = $conn->prepare('SELECT product_name FROM furniture_products WHERE product_id = ?');	
	$stmt2->execute([$deleteProductId]);
	$productName = $stmt2->fetchColumn();
	
	//find requested product by id and delete from database
	$sql = "DELETE FROM furniture_products WHERE product_id = :productId";	
	$stmt3 = $conn->prepare($sql);	//prepare the statement	
	$stmt3->bindParam(':productId',$deleteProductId, PDO::PARAM_INT);	//bind the parameter to the statement
		if ( $stmt3->execute() ){
			$message =  "<h3><span class='check'>&#x2714;</span> Requested product, <em>".$productName."</em> was successfully <span style='color:#b20000;'>deleted</span>.</h3>";	
		}else{
			$message = "<h3><span class='X'>&#x2718;</span> We encountered a problem with your delete request, please try again.</h3>";	
		}
		
	$conn = null;	//close the database connection
	 //if not valid user redirect to login page
}else{
	header('Location: login.php');  
	exit;      
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
	<title>Furniture Co</title>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="files/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">
	<!--Custom CSS--> 
	<link href="files/styles.css" rel="stylesheet">
	<script src="files/jquery-3.2.1.min.js"></script>
	<style>
		.check{
			font-size:1.7em;
			color:#329932;
			font-family: 'Numans', sans-serif;}
		.X{
			font-size:1.7em;
			color:#4c0000;
			font-family: 'Numans', sans-serif;}
		.mainContent{
			text-align:center;
			font-family: 'Numans', sans-serif;}
		@media (max-width: 1200px){
			.row{
				margin-top:8%;}
		}
		@media (max-width: 375px){
			.row{
				margin-top:11%;}
		}
	</style>
</head>
<body>
	
	<nav class="navbar navbar-fixed-top">
		<div class="container-fluid ">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php"><img src="images/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/>Furniture Co</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li><a href="addProduct.php">Add Product</a></li>
				<li><a href="selectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $welcomeMsg?></a></li>
				<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Admin</a></li>
				<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
	
	<div class="container mainContent">
		<div class="row">
			<?php echo $deleteMsg; ?>
			<?php echo $message; ?>
		</div>
	</div> 
	
	<!-- Bootstrap core JavaScript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="files/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>

