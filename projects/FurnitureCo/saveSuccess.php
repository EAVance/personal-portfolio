<?php
session_start();

if (isset($_SESSION['validUser1'])) {       //if valid user you can use this page
	$inUsername = $_SESSION['userName1'];
	$welcomeMsg = "Welcome Back <em>". $inUsername."</em> !"; //displays welcome message once signed in

	$successMsg = "Product Add/Update Successful";
	
}else{
	header('Location: login.php');  
	exit;      
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
	<title>Furniture Co</title>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="files/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">
	<!--Custom CSS--> 
	<link href="files/styles.css" rel="stylesheet">
	<script src="files/jquery-3.2.1.min.js"></script>
	<style>
		.check{
			font-size:1.7em;
			color:#329932;
			font-family: 'Numans', sans-serif;}
		.mainContent{
			text-align:center;
			font-family: 'Numans', sans-serif;}
		@media (max-width: 1200px){
			.row{
				margin-top:8%;}
		}
		@media (max-width: 375px){
			.row{
				margin-top:11%;}
		}
	</style>
</head>
<body>
	
	<nav class="navbar navbar-fixed-top">
		<div class="container-fluid ">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php"><img src="images/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/>Furniture Co</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li><a href="addProduct.php">Add Product</a></li>
				<li><a href="selectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $welcomeMsg?></a></li>
				<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Admin</a></li>
				<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
	
	<div class="container mainContent">
		<div class="row">
			<h3><span class='check'>&#x2714;</span><?php echo $successMsg; ?></h3>
		</div>
	</div> 
	
	<!-- Bootstrap core JavaScript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="files/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>

