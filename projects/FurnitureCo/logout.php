<?php
session_start();	//provide access to the current session

$_SESSION['validUser1']=false;
session_unset();	//remove all session variables related to current session
session_destroy();	//remove current session

header('Location: login.php');  //when logout redirect to login page
exit;
?>