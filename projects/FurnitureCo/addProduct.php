<?php
session_start();

if (isset($_SESSION['validUser1'])) {       
	$inUsername = $_SESSION['userName1'];   //get username              
	$welcomeMsg = "Welcome Back <em>". $inUsername."</em> !";    //welcome message
	
	$inName = $inDescription = $inImage = $inUpdateImage = $inActive = $inDate = $inID = $inPhone = $productImageName = "";    
	$nameError = $descriptionError = $imageError = $imageUploadError = $activeError = $errorMsg = $successMsg = $e = "";  
	$formTitle = "Add Product";
	$validForm = false;

	require_once('files/connectPDO.php');		//CONNECT to the database

	if(empty($inID)){                     
		$formTitle = "Add Product";   //if there is NO product_id change form header to ADD
	}else{
		$formTitle = "Update Product";   //if there is an product_id change form header to UPDATE
	}

	if(isset($_POST["submit"])){ //-------------IF ADD FORM HAS HAS BEEN SUBMITTED, GATHER INPUT, START VALIDATIONS--------------   
		
		$inName = $_POST['product_name'];
		$inDescription = $_POST['product_description'];
		$inImage = $_FILES["photo"]["name"]; 
		$inActive = $_POST['product_active'];
		$inID = $_POST['product_id'];	      //hidden field used for update form
		$inPhone = $_POST['Phone'];
		
		//sanitize name, if empty or just spaces - form is invalid & error message displays
		function validateName(){           
			global $validForm, $nameError, $inName;		
			$inName = filter_var($inName, FILTER_SANITIZE_STRING);
			if(empty($inName)){
				$validForm = false;
				$nameError = "Product Name is Required.";
			}
			return $validForm;
		}
		
		//sanitize description, if empty or spaces - form is invalid & error message displays
		function validateDescription(){
			global $validForm, $descriptionError, $inDescription;
			$inDescription = filter_var($inDescription, FILTER_SANITIZE_STRING);
			if(empty($inDescription)){
				$validForm = false;
				$descriptionError = "Product Description is Required.";
			}
			return $validForm;
		}
	
		//get name of previous uploaded image
		$stmt2 = $conn->prepare('SELECT product_image_name FROM furniture_products WHERE product_id = ?');	
		$stmt2->execute([$inID]);         
		$productImageName = $stmt2->fetchColumn();
				
		//validate upload image
		function validateUploadImage(){
			global $validForm, $imageError, $imageUploadError, $inImage, $inUpdateImage, $productImageName;
			
			if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){            //if updating image file or adding new product with image do this
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
				$filename = $_FILES["photo"]["name"];
				$filetype = $_FILES["photo"]["type"];
				$filesize = $_FILES["photo"]["size"];
				
				// Verify file extension
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!array_key_exists($ext, $allowed))
				{
					$validForm = false;
					$imageUploadError = "Error: Please select a valid file format.";
				}
				
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				if($filesize > $maxsize)
				{
					$validForm = false;
					$imageUploadError = "Error: File size is larger than the allowed limit.";
				}
				
				// Verify MIME type of the file
				if(!in_array($filetype, $allowed))
				{
					$validForm = false;
					$imageUploadError = "Error: Please select a valid file format."; 
				}

				// Check whether file exists before uploading it
				if(file_exists("images/database_product_images/" . $_FILES["photo"]["name"])){
					$validForm = false;
					$imageUploadError = $_FILES["photo"]["name"] . " already exists in desired folder.";
				}

			}else if($_FILES["photo"]["error"] == 1){
				$validForm = false;
				$imageUploadError = "Error: " . $_FILES["photo"]["error"];
			}

			return $validForm;
		}
	
		//validating product active status, if both unchecked - form is invalid & error message displays
		function validateActive(){           
			global $validForm, $activeError, $inActive;		
			if(empty($inActive)){
				$validForm = false;
				$activeError = "Please Select Product Active Status.";
			}
			return $validForm;
		}
		
		//date of product add or update
		function productAddDate(){
			global $inDate;
			date_default_timezone_set('America/Chicago');
			$inDate = date('m/d/Y');
		}
		
		//validate phone, if not blank - form is invalid
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
			return $validForm;
		}
	
		$validForm = true;       
		//calling validation functions
		validateName(); 
		validateDescription();
		validateActive();
		productAddDate();
		validatePhony();
		validateUploadImage();
			
			
		if($validForm){ //-----------------IF VALID FORM MOVE CONTENT TO DATABASE--------------------
			//upload selected image to folder
			move_uploaded_file($_FILES["photo"]["tmp_name"], "images/database_product_images/" . $_FILES["photo"]["name"]);  //move image file to images folder
			$productImageName = '';    //empty previous image var
				
			if(!empty($_POST['product_id'])){ //------------IF PRODUCT UPDATE DO THIS----------------
				if(!empty($inImage)){       //IF UPDATING IMAGE - Create this Update SQL command string
					try {
						$stmt = $conn->prepare("UPDATE furniture_products SET product_name= ?, product_description= ?, product_image_name= ?, date_added= ?, product_active= ? WHERE product_id = ?");
						$isSaved = $stmt->execute(array($inName,$inDescription,$inImage,$inDate,$inActive,$inID));
						
						if ($isSaved){
							header('Location: saveSuccess.php');  
							exit;   
						}

						$conn = null;
					}catch(PDOException $e){
						$errorMsg = "<span class='X'>&#x2718;</span> There has been a problem. Please try again later."; 
					}
				}else{	            //IF NOT UPDATING IMAGE - Create this Update SQL command string
					try {
						$stmt = $conn->prepare("UPDATE furniture_products SET product_name= ?, product_description= ?, date_added= ?, product_active= ? WHERE product_id = ?");
						$isSaved = $stmt->execute(array($inName,$inDescription,$inDate,$inActive,$inID));

					
						if ($isSaved){
							header('Location: saveSuccess.php');  
							exit;   
						}
						
						$conn = null;
					}catch(PDOException $e){
						$errorMsg = "<span class='X'>&#x2718;</span> There has been a problem. Please try again later."; 
					}
				}
			}else{ //------------------IF NEW PRODUCT INSERT DO THIS------------------
				
				try {
					$stmt = $conn->prepare("INSERT INTO furniture_products (product_name, product_description, product_image_name, date_added, product_active) VALUES (?, ?, ?, ?, ?)");
					$isSaved = $stmt->execute(array($inName,$inDescription,$inImage,$inDate,$inActive));

					if ($isSaved){
						header('Location: saveSuccess.php');  
						exit;   
					}
					
					$conn = null;
				}catch(PDOException $e){
					$errorMsg = "<span class='X'>&#x2718;</span> There has been a problem. Please try again later.";
				}
			}	
		}else{ //----------------------IF INVALID FORM DISPLAY ERROR MESSAGE & FORM--------------------------
			$errorMsg = "<span style='color:#b20000;'>Invalid Entry. Please Try Again.</span>";
		}

	}else{ //---------------------IF FORM HAS NOT BEEN SEEN AND NOT SUBMITTED DO THIS------------------------                         
		$formTitle = "Add Product";   //if there is no product_id change form header to ADD
		
		if(isset($_GET['product_id'])){ //-------------------IF PRODUCT ID IS PRESENT FROM $_GET, SHOW PRODUCT DATA FOR UPDATE----------------
			$inID = $_GET['product_id'];
			$formTitle = "Update Product";    //if there is an product_id change form header to UPDATE
			try {
				
				$stmt = $conn->prepare("SELECT product_name, product_description, product_image_name, product_active FROM furniture_products WHERE product_id =?");
				$stmt->execute(array($inID));

				if ($stmt->rowCount() == 1) 
				{
					$dataArray = $stmt->fetch(PDO::FETCH_ASSOC);
					$inName = $dataArray["product_name"];
					$inDescription = $dataArray["product_description"];
					$inUpdateImage = $dataArray["product_image_name"]; 
					$inActive = $dataArray["product_active"];
				
				}else {
					$displayErrorMsg = "<h3><span class='X'>&#x2718;</span><em> Zero results were found</em></h3>";			
				}		
				
				$conn = null;
			}catch(PDOException $e){
				$displayErrorMsg = "<h3><span class='X'>&#x2718;</span><em> Sorry there has been a problem.</em></h3>";
			}
		}
	}
	//if not a valid user redirect to login page
}else{
	header('Location: login.php');  
	exit;       
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='shortcut icon' type='image/png' href='../../images/favicon.png' />
	<title>Furniture Co</title>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="files/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">	
	<!--Custom CSS--> 
	<link href="files/styles.css" rel="stylesheet">
	<link href="files/addProductStyles.css" rel="stylesheet">
	<!--JS-->
	<script src="files/jquery-3.2.1.min.js"></script>
</head>
<body>
	<!--Navigation-->
	<nav class="navbar navbar-fixed-top"> 
		<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php"><img src="images/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li class="active"><a href="addProduct.php">Add Product</a></li>
				<li><a href="selectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $welcomeMsg?></a></li>
				<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Admin</a></li>
				<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
	
	<div class="container mainContent">
		<h2><?php echo $formTitle;?></h2>
		<h3><em><?php echo $successMsg; ?><?php echo $errorMsg; ?></em></h3>
		<!--Form section-->		
		<form  id="productForm" name="productForm" method="post"  enctype="multipart/form-data" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
					
			<p>
				<label>Product Name <br><span class="error"><?php echo $nameError; ?></span></label>
				<input type="text" name="product_name" id="eventName" value="<?php echo $inName;?>">
			</p>
			<p>
				<label>Product Description <br><span class="error"><?php echo $descriptionError; ?></span></label>
				<textarea  name="product_description" id="description" rows="5" ><?php echo $inDescription;?></textarea>
			</p>
			<p>
				<label>Select Product Image<br><span class="error"><?php echo $imageError; ?><?php echo $imageUploadError; ?></span></label>
				<br><span class="note"><em> Only .jpg, .jpeg, .gif, .png formats allowed. Max size 5 MB</em></span>
				<br><span class="prevUploadedImage">Previous Uploaded Image: <em><?php echo $inUpdateImage;?></em></span>
					<input type="file" name="photo" id="fileSelect"><label for="fileSelect"></label>
			</p>	
			<p>	
				<label>Do you want this product to be currently active? <br><span class="error"><?php echo $activeError; ?></span></label>
				<span class="radio">
					<input type="radio" name="product_active" value="yes" <?php if ($inActive == 'yes') { echo "checked"; } ?>/> Yes
					<input type="radio" name="product_active" value="no" <?php if ($inActive == 'no') { echo "checked"; } ?>/> No 
				</span>
			</p>
			<input type="hidden" name="product_id" id="id" value="<?php echo $inID; ?>" />
			<div id="phony">
				<label>Phone:</label>
				<input type="text" name="Phone">
			</div>											
			<div class="formButtons">
				<input type="submit" name="submit" value="SUBMIT" id="submit" class="btn">
				<input type="reset" name="reset" value="RESET" id="reset" class="btn">
			</div>
		</form><!--end form-->
	</div> <!-- /container -->
	
	<!-- Bootstrap core JavaScript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="files/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>