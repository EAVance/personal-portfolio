<?php
require_once('php/contact-form.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Erin Vance | Web Development</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=0">
  <meta name="description" content="Aspiring Front-End Web Developer &amp; Designer."/>
  	<link rel='shortcut icon' type='image/png' href='images/favicon.png' />
	<!-- CSS -->
	<link href="css/nameOutlineText.css" rel="stylesheet">
	<link href="css/bgGradientAnimation.css" rel="stylesheet">
	<link href="css/portfolioStyles.css" rel="stylesheet">
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:900i|Prompt" rel="stylesheet">
	<!-- JavaScript -->
	<script src="javascript/jquery-3.3.1.min.js"></script>
	<!-- Intro Name Outline JS -->
	<script src="javascript/nameOutlineText.js"></script>
	<script>
		$(document).ready(function () {	
		 //on nav links or up button click animated easy scroll to section
			$("a[href^='#']").click(function (e) {
				e.preventDefault();

				var position = $($(this).attr("href")).offset().top - 10;
					
				$("body, html").animate({
					scrollTop: position
				}, 1200);
			});
			
		 //show the animation navbar on scroll
			$(window).scroll(function() {
				if ($(".navbar").offset().top > 50) {
					$(".navbar").addClass("nav-scroll");  		
				} else {
					$(".navbar").removeClass("nav-scroll");		
				}
			 //when scrolled to section navbar reflects which section you are at by adding .active class to nav link
				var scrollPos = $(document).scrollTop();  //how many pixels you've scrolled from top
				$(".nav-scroll a").each(function () {
					//var currLink = $(this);
					var refElement = $($(this).attr("href")); 
					if ((refElement.position().top) - 75 <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
						$(".nav-scroll .navbar-links-container ul li a").removeClass("active"); 
						$(this).addClass("active");   
					} else {
						$(this).removeClass("active");
					}
				});
			});
    	});
	</script>
</head>
<body id="page-top">

<!--  Navigation  -->
    <nav class="navbar">
      	<div class="navbar-header">
			<a class="navbar-brand" href="#page-top"><span id="navbar-brand-lg">Erin Vance</span><span id="navbar-brand-sm">EV</span></a>
		</div>
		<div class="navbar-links-container">
    		<ul class="navbar-links">
				<li><a class="link" href="#about">About</a></li>
				<li><a class="link" href="#projects">Projects</a></li>
				<li><a class="link" href="#contact">Contact</a></li>
			</ul>
		</div>
		<div class="bgGradient1"></div>
		<div class="bgGradient2"></div>
		<div class="bgGradient3"></div>
    </nav><!--End Navigation-->
	
<!--  Intro Section  -->
    <div id="intro" class="intro-section bgGradientSection">
		<div class="bgGradient1"></div>
		<div class="bgGradient2"></div>
		<div class="bgGradient3"></div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="me">Erin Vance</h1>
				<h2 class="fadeInText">Aspiring Front-End Web Developer &amp; Designer</h2>
			</div>
		</div>
		<div class="row fadeInBtn">
			<a href="#about" class="pulseBtnArrow">&#x2022;</a>
		</div>
    </div><!--End Intro Section-->
	
<!--  About Section  -->
    <div id="about" class="about-section">
		<h2>About</h2>
        <div class="row">
			<div class="col-3">
				<img class="img-responsive" src="images/html.png" alt="html icon">
				<img class="img-responsive" src="images/css.png" alt="css icon">
				<img class="img-responsive" src="images/javascript.png" alt="javascript icon">
				<img class="img-responsive" src="images/sass.png" alt="sass icon">
				<img class="img-responsive" src="images/PS.png" alt="photoshop icon">
				<img class="img-responsive" src="images/Bootstrap.png" alt="bootstrap icon">
				<img class="img-responsive" src="images/php1.png" alt="php icon">
				<img class="img-responsive" src="images/wordpresslogo1.png" alt="wordpress icon">
			</div>
          	<div class="col-9">
				<p>Originally my education started with studying Industrial Design and Metalsmithing/Jewelry Design at the University of Kansas where I learned core 
					design principles and transformed my ideas from design stage to final product.</p>
				<p>After some time out of KU I found myself yearning to create again.  I soon started dabbling in online HTML and CSS courses where I instantly found myself captivated.  
					So I immersed myself back in school pursuing a degree in the ever evolving and fascinating Web Development field at DMACC.</p>
				<p>Once completing the program, I can now say I have experience with a variety of web development languages, frameworks, concepts and more. Such as <strong><em>HTML5</em></strong>, <strong><em>CSS3</em></strong>, <strong><em>JavaScript</em></strong>, 
					<strong><em>jQuery</em></strong>, <strong><em>PHP</em></strong>, <strong><em>SASS</em></strong>, <strong><em>Bootstrap</em></strong>, <strong><em>Responsive layouts</em></strong>, 
					<strong><em>WordPress customization</em></strong>, <strong><em>git</em></strong>, <strong><em>MVC</em></strong> and <strong><em>Photoshop</em></strong>.  In this field I have realized there is always room to learn, improve and evolve.   So, I am currently learning 
					<em>Drupal</em>, a web content management platform and also some test automation with <em>Selenium WebDriver</em>.  My objective is to become a successful Front-End Web Developer that can design and develop a website that is functional, 
					user centered, and creative.</p>
			</div>
        </div>
		<div class="row">
			<a href="#projects" class="pulseBtnArrow">&#x2022;</a>
		</div>
    </div><!--End About Section-->
	
<!--  Projects Section  -->
    <div id="projects" class="projects-section"> 
		<h2>Projects</h2>
      	<div class="row">
          	<div class="col-3">
				<a href="http://erinvance.info/projects/SimplyYummy/index.html" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/simplyYummy_2.png" alt="Simply Yummy Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">Simply Yummy Bites</h4>
						<p class="card-text"><strong>Advanced JavaScript</strong><br>Dynamic recipe website, that uses JSON objects, AJAX calls, jQuery &amp; cookies.</p>
					</div>
				</div>
				</a>
        	</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/FurnitureCo/index.php" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/furnitureCo.png" alt="Intro PHP Furniture Co. Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">Furniture Co.</h4>
						<p class="card-text"><strong>Intro PHP</strong><br>Custom CMS furniture website that allows administration to update/edit products and uses PHP &amp; PDO_MYSQL.
							<br><span class="guestLogin">VISITER LOGIN <br> <strong>Username:</strong> <em>guest </em> <strong> Password:</strong> <em>guest</em></span>
						</p>
					</div>
				</div>
				</a>
			</div>
			<div class="col-3">
				<a href="http://whatyoudontsee.com/" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/whatYouDontSee.png" alt="WordPress Group Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">What You Don't See</h4>
						<p class="card-text"><strong>Web Application Seminar</strong><br>Team collaboration project for the Iowa Narcotics Officers Association, migrated existing WordPress site, updated and improved responsiveness.</p>
					</div>
				</div>
				</a>
			</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/DmaccPortfolioDay/index.html" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/dmaccPortfolioDay_2.png" alt="DMACC Portfolio Day Group Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">DMACC Portfolio Day</h4>
						<p class="card-text"><strong>Web Application Seminar</strong><br>Team collaboration project that uses CSS animations, AJAX calls, and JQuery.</p>
					</div>
				</div>
				</a>
			</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/BEERate/login.php" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/beeRate.png" alt="Advanced JavaScript BEERate Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">BEERate</h4>
						<p class="card-text"><strong>Advanced JavaScript</strong><br>Personal beer rating project that uses jQuery UI, PHP &amp; PDO_MYSQL.
							<br><span class="guestLogin">VISITER LOGIN <br> <strong>Username:</strong> <em>guest </em> <strong> Password:</strong> <em>guest</em></span>
						</p>
					</div>
				</div>
				</a>
        	</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/DataVisualization/index.html" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/dataVisualization.png" alt="Data Visualization">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">Data Visualization</h4>
						<p class="card-text"><strong>Web Application Components</strong><br>Data visualizations using Plotly.js Scatter Plot, Google Charts GeoChart, &amp; Google Maps API Heatmap.</p>
					</div>
				</div>
				</a>
        	</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/BeyondAdoredPrototype/index.html" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/beyondAdoredPrototype_2.png" alt="Beyond Adored Prototype">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">Beyond Adored Prototype</h4>
						<p class="card-text"><strong>Intro Web Design</strong><br>Prototype designed in Adobe Photoshop, uses the basic elements &amp; principles of design.</p>
					</div>
				</div>
				</a>
        	</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/BeyondAdoredWeb/index.html" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/beyondAdoredWeb_2.png" alt="Beyond Adored Boostrap Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">Beyond Adored</h4>
						<p class="card-text"><strong>Advanced CSS Project</strong><br>Uses Bootstrap framework features for a responsive design layout.</p>
					</div>
				</div>
				</a>   
        	</div>
			<div class="col-3">
				<a href="http://erinvance.info/projects/ZoomNGroom/index.html" title="View Project" target="_blank">
				<div class="card">
					<img class="card-img-top img-responsive" src="images/zoomNgroom_3.png" alt="Media Queries Project">
					<div class="card-block">
						<img src="images/viewIcon.png" alt="View Project" title="View Project" class="card-icon">
						<h4 class="card-title">Zoom &amp; Groom</h4>
						<p class="card-text"><strong>Advanced CSS Project</strong><br>Implements custom media queries for a responsive design.</p>
					</div>
				</div>
				</a>
        	</div>
		</div>
		<div class="row">
			<a href="#contact" class="pulseBtnArrow">&#x2022;</a>
		</div>
    </div><!--End Project Section-->
	
<!--  Contact Section  -->
    <div id="contact" class="contact-section">
		<h2>Wanna get in touch?</h2>
		<h3>Feel free to contact me via email <strong>erinavance@yahoo.com</strong><br><em>or</em> drop me a line below.</h3>
		<div class="repoLinks">
			<a href="https://bitbucket.org/EAVance/" target="_blank"><img src="images/bitbucket_2.png" alt="Bitbucket Repo" title="My Repo">Repo</a>
			<a href="http://erinvance.info/resume/ErinVanceResume.pdf" target="_blank"><img src="images/resume_2.png" alt="My Resume" title="My Resume">Resume</a>	
		</div>
      	<form id="contact-form" action="<?php echo $_SERVER['SCRIPT_NAME'].'#contact'; ?>" method="post">
			<div class="row">
				<div class="col-12">
					<label class="">Name
						<?php if (isset($contactErrorsArray['contactName'])) 
						{ ?>
							<span id="errors"><?php echo $contactErrorsArray['contactName'];?></span> 
						<?php } ?>
					</label>
					<input type="text" name="contactName" value="<?php echo echoValue($contactDataArray, 'contactName'); ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<label class="">Email
						<?php if (isset($contactErrorsArray['contactEmail'])) 
						{ ?>
							<span id="errors"><?php echo $contactErrorsArray['contactEmail'];?></span> 
						<?php } ?>
					</label>
					<input type="text" placeholder="abc123@yahoo.com" name="contactEmail" value="<?php echo echoValue($contactDataArray, 'contactEmail'); ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<label class="">Phone
						<?php if (isset($contactErrorsArray['contactPhone'])) 
						{ ?>
							<span id="errors"><?php echo $contactErrorsArray['contactPhone'];?></span> 
						<?php } ?>
					</label>
					<input type="text" placeholder="555-555-5555" name="contactPhone" value="<?php echo echoValue($contactDataArray, 'contactPhone'); ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<label class="">Questions
						<?php if (isset($contactErrorsArray['contactQuestions'])) 
						{ ?>
							<span id="errors"><?php echo $contactErrorsArray['contactQuestions'];?></span> 
						<?php } ?>
					</label>
					<textarea id="commentTextarea" name="contactQuestions" rows="3"><?php echo echoValue($contactDataArray, 'contactQuestions'); ?></textarea>
				</div>
			</div>
			<input type="hidden" name="contactID" value="<?php echo echoValue($contactDataArray, 'contactID'); ?>"/>
			<div class="row">
				<input type="submit" name="submit" value="Submit"/>
				<input type="reset" name="reset" value="Reset"/>
			</div>
		</form>
		<a id="topButton" class="fadeInBtn" href="#intro">^</a> 
		<p class="copyright">
			&#169;
			<script type="text/javascript">
				document.write(new Date().getFullYear());
			</script>
			Erin Vance Web Development <span class="bar"> | </span> All Rights Reserved
		</p>
  	</div><!--End Contact Section-->

</body>
</html>